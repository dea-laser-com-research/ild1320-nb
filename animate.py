import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import datetime as dt
import threading
import io
import serial


class ILD1320:
    def __init__(self, stream):
        self.stream = stream

    def write(self, string):
        self.stream.write(string.encode('ascii') + b'\r\n')

    def observe(self):
        while True: # until we have low (first) byte
            l = int.from_bytes(s.read(1), byteorder="big")
            #print(format(l, '#010b'))
            if ((l >> 6) != 0): continue

            m = int.from_bytes(s.read(1), byteorder="big")
            #print(format(m, '#010b'))
            if ((m >> 6) != 1): continue

            h = int.from_bytes(s.read(1), byteorder="big")
            #print(format(h, '#010b'))
            if ((h >> 6) != 2): continue

            # currently inelegant solution to buffer problem
            s.reset_input_buffer()
            return ((h & 0x3f) << 12) | ((m & 0x3f) << 6) | (l & 0x3f)

    def distance(self, mr=100, smr=50):
        return ((1/100)*(((102*self.observe())/65520)-1)*mr) + smr

s = serial.Serial('/dev/ttyUSB1', baudrate=921600, timeout=0)    
sensor = ILD1320(s)

sensor.write( 'TRIGGER NONE')
sensor.write( 'OUTHOLD INFINITE')
sensor.write( 'MEASPEAK DISTA')
sensor.write( 'MEASRATE 2')
sensor.write( 'OUTPUT RS422')
sensor.write( 'ECHO OFF')
sensor.write( 'MASTERMV NONE')
sensor.write( 'LASERPOW FULL')
# sensor.write( 'BAUDRATE 1000000')

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
xs = [0] * 20
ys = [1] * 20

def animate(i, xs, ys):
    d = round(sensor.distance(), 4)
    xs.append(dt.datetime.now().strftime('%H:%M:%S.%f'))
    ys.append(d)
    
    xs = xs[-20:]
    ys = ys[-20:]

    ax.clear()
    ax.plot(xs, ys)
    
    plt.xticks(rotation=45, ha='right')
    plt.ylim(0, 200)
    plt.subplots_adjust(bottom=0.30)
    plt.title('Displacement')
    plt.ylabel('Displacement (mm)')
    
ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys), interval=100)
plt.show()

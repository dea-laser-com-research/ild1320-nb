{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# DEA-LaserCom research notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ILD1320 sensor interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See the [operation manual](https://www.micro-epsilon.com/download/manuals/man--optoNCDT-1320--en.pdf) for this sensor for the references cited throughout this document."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import clear_output\n",
    "import time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's first install [pySerial](https://pythonhosted.org/pyserial/pyserial.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    import serial\n",
    "except:\n",
    "    import sys\n",
    "    !{sys.executable} -m pip install pyserial --user\n",
    "    import serial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This class gives us an interface for working with bidirectional streams."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "class StreamInterface:\n",
    "    def __init__(self, stream, encoding, eol):\n",
    "        self.stream = stream\n",
    "        self.encoding = encoding\n",
    "        self.eol = eol.encode(self.encoding)\n",
    "        \n",
    "    def write(self, message, return_response=False):\n",
    "        self.stream.write(message.encode(self.encoding) + self.eol)\n",
    "        if return_response:\n",
    "            if isinstance(return_response, int): return self.stream.read(return_response)\n",
    "            else: return self.stream.readlines()\n",
    "        else: return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To communicate with the ILD1320 sensor, we are using 8 data bits and no stop bits. The data are sent from the sensor as three-byte packets, low-to-high:\n",
    "\n",
    "|         |   |   |   |   |   |   |   |   |   |\n",
    "|---------|---|---|---|---|---|---|---|---|---|\n",
    "| $L$-Byte | $0$ | $0$ || $D5$ | $D4$ | $D3$ | $D2$ | $D1$ | $D0$ |\n",
    "| $M$-Byte | $0$ | $1$ || $D11$ | $D10$ | $D9$ | $D8$ | $D7$ | $D6$ |\n",
    "| $H$-Byte | $1$ | $0$ || $(D^*)$ | $(D17)$ | $(D16)$ | $D15 $| $D14$ | $D13$ |\n",
    "\n",
    "The $D$-bits are put together in decreasing order of index (big-endian). Every observation cycle, each output parameter is individually sent in such a three-byte block, with effective data width between 16 and 18 bits ($D0$ - $D16, D18$). Since multiple output parameters can be chosen via `OUTADD_RS422` (Appendix A 3.5.3.2) for each observation cycle, bit $D^*$ is $0$ when the current three-byte block is the first in its observation cycle.\n",
    "See section 7.2.2 of the [operation manual](https://www.micro-epsilon.com/download/manuals/man--optoNCDT-1320--en.pdf) for more details on the encoding scheme.\n",
    "\n",
    "Using the formula provided in section 7.2.2, we can calculate the distance in millimeters. The `observe` function returns the $x$ in that expression. Measuring range is given in millimeters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "class ILD1320(StreamInterface):\n",
    "    def __init__(self, stream, encoding='ascii', eol='\\r\\n', **kwargs):\n",
    "        super().__init__(stream, encoding, eol)\n",
    "        self.__dict__.update({\n",
    "            \"measuring_range\": 100,\n",
    "            \"start_of_measuring_range\": 50\n",
    "        })\n",
    "        self.__dict__.update(kwargs)\n",
    "    \n",
    "    def observe(self):\n",
    "        while True: # until we get the first low-order byte\n",
    "            l = int.from_bytes(self.stream.read(1), byteorder=\"big\")\n",
    "            #print(format(l, '#010b'))\n",
    "            if ((l >> 6) != 0): continue\n",
    "        \n",
    "            m = int.from_bytes(self.stream.read(1), byteorder=\"big\")\n",
    "            #print(format(m, '#010b'))\n",
    "            if ((m >> 6) != 1): continue\n",
    "            \n",
    "            h = int.from_bytes(self.stream.read(1), byteorder=\"big\")\n",
    "            #print(format(h, '#010b'))\n",
    "            if ((h >> 6) != 2): continue\n",
    "        \n",
    "            # currently inelegant solution to buffer problem\n",
    "            self.stream.reset_input_buffer()\n",
    "            return ((h & 0x3f) << 12) | ((m & 0x3f) << 6) | (l & 0x3f)\n",
    "    \n",
    "    def distance(self, sleep=0):\n",
    "        if sleep > 0: time.sleep(sleep)\n",
    "        return ((1/100)*(((102*self.observe())/65520)-1)*self.measuring_range) + self.start_of_measuring_range"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By using the `by-id` directory, which simlinks info `/dev/tty`, you can reference the serial device by its unique ID."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "ild1320_stream = serial.Serial(\n",
    "    '/dev/serial/by-id/usb-Micro-Epsilon_Micro-Epsilon_IF2001_USB_002988-if00-port0', \n",
    "    baudrate=921000, timeout=0\n",
    ")\n",
    "\n",
    "ild1320 = ILD1320(ild1320_stream)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mastering is disabled until I can create an object-oriented implementation. We probably don't need it for our measurements, anyway."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "for command in ['TRIGGER NONE', 'OUTHOLD NONE', 'MEASPEAK DISTA', 'MEASRATE 2', \n",
    "                'OUTPUT RS422', 'ECHO OFF', 'MASTERMV NONE', 'LASERPOW FULL']:\n",
    "    ild1320.write(command)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the formula provided in section 7.2.2, we can calculate the distance in millimeters. The function above returns the `x` in that expression. Measuring range is given in millimeters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's an infinite loop (with updating) to show raw distances from the sensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "109.19\n",
      "0\n"
     ]
    }
   ],
   "source": [
    "def ild1320_continuous_measurement():\n",
    "    try:\n",
    "        while True:\n",
    "            clear_output(wait=True)\n",
    "            print(round(ild1320.distance(), 2))\n",
    "            print(ild1320.stream.in_waiting)\n",
    "            time.sleep(0.09)\n",
    "    except KeyboardInterrupt:\n",
    "        clear_output(wait=True)\n",
    "        \n",
    "ild1320_continuous_measurement()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we integrate the with the ESP32 microcontroller that controls PWM voltage output to the actuator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dielectric elastomer actuator remote interface"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "class DEAController(StreamInterface):\n",
    "    def __init__(self, stream, encoding='ascii', eol='', **kwargs):\n",
    "        super().__init__(stream, encoding, eol)\n",
    "        \n",
    "    def pwm(self, i):\n",
    "        frmt = \"A{aDuty:.2f}B{bDuty:.2f}C{cDuty:.2f}D{dDuty:.2f}\"\n",
    "        \n",
    "        if (i >= 0):\n",
    "            self.write(frmt.format(aDuty=i, bDuty=i, cDuty=0, dDuty=0))\n",
    "        else:\n",
    "            self.write(frmt.format(aDuty=0, bDuty=0, cDuty=abs(i), dDuty=abs(i)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "dea_stream = serial.Serial(\n",
    "    '/dev/serial/by-id/usb-Silicon_Labs_CP2102N_USB_to_UART_Bridge_Controller_e22272fd3499e8119165bc20c3e5cfbd-if00-port0', \n",
    "    baudrate=921600, timeout=0\n",
    ")\n",
    "\n",
    "dea = DEAController(dea_stream)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We address the microcontroller with a string encoding a floating-point number that gives the desired duty cycle from `full_duty_voltage` kV. We need two archetypal loops: an inner loop for measuring elastomer displacement every `sample_intr` seconds, up to `rest_intr` seconds, with a constant duty cycle; and an outer loop for adjusting the duty cycle every `rest_intr` seconds and maintaining continuous sampling throughout by calling the inner loop.\n",
    "\n",
    "Note that sampling happens essentially continuously, every `sample_intr` seconds. So time is kept discrete, but it can be easily calculated from the parameters above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "import csv\n",
    "import functools\n",
    "from datetime import datetime\n",
    "\n",
    "class DEATester:\n",
    "    def __init__(self, dea, sensor, intervals={}):\n",
    "        self.dea = dea\n",
    "        self.sensor = sensor\n",
    "        self.intervals = intervals\n",
    "        self.figsize = (7, 5)\n",
    "        self.seconds_left = 0\n",
    "        self.reset()\n",
    "        \n",
    "    def reset(self):\n",
    "        self.xs = []\n",
    "        self.ys = []\n",
    "        self.seconds_left = 0\n",
    "\n",
    "    def measure_loop(self, rest=None):\n",
    "        clear_output(wait=True)\n",
    "        if rest is None: rest = self.pwm_range[0]\n",
    "        samples = int(self.intervals['hold']/self.intervals['sample'])\n",
    "        self.dea.pwm(rest)\n",
    "        for i in range(samples):\n",
    "            print(\"duty cycle: {}\\nsamples: {} / {}\".format(rest, i + 1, samples))\n",
    "            dist = round(self.sensor.distance(), 4)\n",
    "            if dist == 456.9938:\n",
    "                raise ValueError(\"Sensor entered error state\")\n",
    "                \n",
    "            print(\"observation: {} mm\".format(dist))\n",
    "            tester.ys.append(dist)\n",
    "            tester.xs.append(rest)\n",
    "            time.sleep(self.intervals['sample'])\n",
    "            clear_output(wait=True)\n",
    "            \n",
    "    def increment_loop(self, range=[0., 100], reverse=False):\n",
    "        pwm_range = np.arange(*range, -self.intervals['pwm'] if reverse else self.intervals['pwm'])\n",
    "        self.seconds_left = 0\n",
    "        for pwm in pwm_range:\n",
    "            if self.intervals['hold'] > 0: \n",
    "                self.measure_loop(pwm)\n",
    "                \n",
    "            clear_output(wait=True)\n",
    "            if self.intervals['rest'] > 0:\n",
    "                self.dea.pwm(0)\n",
    "                print(\"resting: {} duty cycle, {} sec\".format(0, self.intervals['rest']))\n",
    "                time.sleep(self.intervals['rest'])\n",
    "                \n",
    "\n",
    "    def export(self, prefix=\"data\"):\n",
    "        with open(\n",
    "            \"{}-{}-{}-{}-{}-{}.csv\".format(\n",
    "                prefix,\n",
    "                time.strftime(\"%Y%m%d-%H%M%S\"),\n",
    "                *[interval for interval in self.intervals.values()]\n",
    "            ), 'w') as csvfile:\n",
    "            wr = csv.writer(csvfile)\n",
    "            for x, y in zip(self.xs, self.ys):\n",
    "                wr.writerow([x, y])\n",
    "                \n",
    "    def to_df(self):\n",
    "        return pd.DataFrame({\"t\": range(len(self.xs)), \"d\": self.ys, \"v\": self.xs})\n",
    "\n",
    "tester = DEATester(dea, ild1320)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we cycle actuation up and down, with measurement at the extremes to account for viscoelasticity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "duty cycle: -65.0\n",
      "samples: 10 / 10\n",
      "observation: 108.3474 mm\n"
     ]
    }
   ],
   "source": [
    "tester.intervals = {\n",
    "    \"sample\": 0.1,\n",
    "    \"rest\": 0,\n",
    "    \"hold\": 1,\n",
    "    \"pwm\": 5\n",
    "}\n",
    "\n",
    "tester.reset()\n",
    "loops = 1\n",
    "try:\n",
    "    for loop in range(loops):\n",
    "        #tester.increment_loop([100., -100.])\n",
    "        #tester.increment_loop([100., -100.], reverse=True)\n",
    "        tester.increment_loop([-70., 105.])\n",
    "        tester.increment_loop([100., -70.], reverse=True)\n",
    "except KeyboardInterrupt:\n",
    "    tester.dea.pwm(0)\n",
    "except:\n",
    "    tester.dea.pwm(0)\n",
    "    raise\n",
    "    \n",
    "dea.pwm(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "dea.pwm(100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we are keeping duty cycle constant, we can just plot displacement against time:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    " \n",
    "\n",
    " \n",
    "\n",
    " \n",
    "\n",
    " \n",
    "\n",
    " \n",
    "\n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.scatter(range(len(tester.ys)), tester.ys, c=tester.xs, marker='.');\n",
    "plt.title(\"Time vs displacement\");\n",
    "plt.colorbar(label=\"duty cycle (%)\");\n",
    "plt.xlabel(\"timesteps ({} sec)\".format(tester.intervals['sample']));\n",
    "plt.ylabel(\"displacement (mm)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.scatter(tester.ys, tester.xs, c=range(len(tester.ys)), marker='.');\n",
    "plt.title(\"Displacement vs duty cycle\");\n",
    "plt.colorbar(label=\"timesteps ({} sec)\".format(tester.intervals['sample']));\n",
    "plt.ylabel(\"duty cycle (%)\");\n",
    "plt.xlabel(\"displacement (mm)\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the data from a full test, we can generate a 3D plot of time versus voltage versus displacement to see viscoelasticity in action."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from mpl_toolkits import mplot3d\n",
    "\n",
    "ax = plt.axes(projection='3d')\n",
    "ax.scatter3D(tester.ys, range(len(tester.ys)), tester.xs, c='g', cmap='Greens');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Machine learning adventures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Basic polynomial fitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We wish to model a function $d(v, t)$ that gives voltage as a function of time and desired displacement. We will follow the path described in \"Deep Reinforcement Learning in Soft Viscoelastic Elastomer\" (Li et al, 2019). The authors fit a 4-th order polynomial $v = P_t(d)$ to voltage-displacement pairs at each given point in time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To obtain this data, we will first gather data on 5% voltage steps for 15 seconds, with a 10 second rest period in between each actuation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "intervals = {\n",
    "    \"sample\": 0.1,\n",
    "    \"rest\": 10,\n",
    "    \"hold\": 40,\n",
    "    \"pwm\": 5\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "duty cycle: 40.0\n",
      "samples: 80 / 400\n",
      "observation: 110.2187 mm\n"
     ]
    }
   ],
   "source": [
    "tester.intervals = intervals\n",
    "\n",
    "tester.reset()\n",
    "loops = 1\n",
    "try:\n",
    "    for loop in range(loops):\n",
    "        tester.increment_loop([40., 105.])\n",
    "        \n",
    "    tester.export()\n",
    "except KeyboardInterrupt:\n",
    "    tester.dea.pwm(0)\n",
    "except:\n",
    "    tester.dea.pwm(0)\n",
    "    raise\n",
    "\n",
    "dea.pwm(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "dea.pwm(100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's examine the difference between taking a break and not taking a break:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_break = pd.read_csv(\"data-20200606-153135-0.5-5-15-5.csv\", names=['v', 'd'])\n",
    "df_break['t'] = df_break.index\n",
    "\n",
    "df_no_break = pd.read_csv(\"data-20200606-153445-0.5-0-15-5.csv\", names=['v', 'd'])\n",
    "df_no_break['t'] = df_no_break.index\n",
    "\n",
    "plt.scatter(df_no_break.t, df_no_break.d)\n",
    "plt.scatter(df_break.t, df_break.d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to import data directly from the CSV that we wisely created above. Note that the timestamps must be recreated, as they are not stored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "datadir = \"/home/carson/research/nathanael-ml/datafiles/\"\n",
    "filename = \"data-20200614-180546-0.1-15-30-2.5.csv\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#df = tester.to_df()\n",
    "\n",
    "df = pd.read_csv(datadir + filename, names=['v', 'd'], header=None)\n",
    "df['t'] = df.index"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because of the way the files are stored, we can also parse the filenames to get the parameters back out easily."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import parse\n",
    "\n",
    "parsed = parse.parse(\"{}-{}-{}-{}-{}-{}-{}.csv\", filename)\n",
    "intervals = {\n",
    "    \"sample\": float(parsed[3]),\n",
    "    \"rest\": float(parsed[4]),\n",
    "    \"hold\": float(parsed[5]),\n",
    "    \"pwm\": float(parsed[6])\n",
    "}\n",
    "intervals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['t'] = df['t'].apply(lambda x: x % (intervals['hold'] / intervals['sample']))\n",
    "groups = df.groupby('t')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For debugging purposes, drop the first and the last entries in each group."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy.polynomial import Polynomial\n",
    "\n",
    "_polys = groups.apply(lambda x: Polynomial.fit(x.d[1:-1], x.v[1:-1], 5)).to_list()\n",
    "print(\"Samples: {}\\nCurves:  {}\".format(len(df.v), len(_polys)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "polyss = []\n",
    "rmses = []\n",
    "\n",
    "orders = range(2, len(df.v) // len(groups) + 1)\n",
    "\n",
    "for order in orders:\n",
    "    clear_output(wait=True)\n",
    "    print(\"Fitting degrees: {} / {}\".format(order, len(df.v) // len(groups)))\n",
    "    polys = groups.apply(lambda x: Polynomial.fit(x.d[1:-1], x.v[1:-1], order)).to_list()\n",
    "    polyss.append(polys)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams['figure.figsize'] = [10, 10]\n",
    "\n",
    "polyss = polyss[:6]\n",
    "\n",
    "fig, ax = plt.subplots(1, len(polyss), sharex=True, sharey=True);\n",
    "fig.add_subplot(111, frameon=False)\n",
    "plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)\n",
    "\n",
    "fig.suptitle(\"$v = P_t(d)$\")\n",
    "plt.xlabel(\"$d$: displacement (mm)\")\n",
    "plt.ylabel(\"$v$: duty cycle (%)\")\n",
    "\n",
    "d_min = df.d.min()\n",
    "d_max = df.d.max()\n",
    "v_max = df.v.max()\n",
    "offset = 1\n",
    "\n",
    "print(\"Displacements: {} mm -> {} mm\".format(d_min, d_max))\n",
    "\n",
    "i = 0\n",
    "for polys in polyss:\n",
    "    #rmse = []\n",
    "    #for poly, group in zip(polys, groups):\n",
    "    #    ax[i].plot(*(poly.linspace()))\n",
    "    #    ax[i].set_xlim(left=d_min - offset, right=d_max + offset)\n",
    "    #    ax[i].set_ylim(top=110, bottom=df.v.min() + 5)\n",
    "    #    \n",
    "    #    break\n",
    "    \n",
    "    t = 220\n",
    "\n",
    "    ax[i].plot(*(polys[t].linspace()))\n",
    "\n",
    "    points = groups.get_group(t)\n",
    "    ax[i].scatter(points.d, points.v)\n",
    "    ax[i].set_xlim(left=105, right = 120)\n",
    "    #plt.title(\"Displacement v. duty cycle at time {}\".format(t))\n",
    "    #plt.xlabel(\"$d$: displacement (mm)\");\n",
    "    #plt.ylabel(\"$v$: duty cycle (%)\");\n",
    "    \n",
    "    i += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see how our fitted plolynomials look. First let's look at one polynomial, and then see them all."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = 6\n",
    "\n",
    "plt.plot(*(_polys[t].linspace()))\n",
    "\n",
    "points = groups.get_group(t)\n",
    "plt.scatter(points.d, points.v)\n",
    "plt.xlim(left=105, right = 120)\n",
    "plt.title(\"Displacement v. duty cycle at time {}\".format(t))\n",
    "plt.xlabel(\"$d$: displacement (mm)\");\n",
    "plt.ylabel(\"$v$: duty cycle (%)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for poly, group in zip(_polys, groups):\n",
    "    plt.title(\"$v = P_t(d)$\")\n",
    "    plt.plot(*(poly.linspace()))\n",
    "    plt.ylim(top=110, bottom=0)\n",
    "    plt.xlabel(\"$d$: displacement (mm)\")\n",
    "    plt.ylabel(\"$v$: duty cycle (%)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try to replicate the experiment shown in the Singapore paper: Generate a sine wave, and see how well the elastomer can follow it with this polynomial approximation alone. We use a simple measuring framework, with one-to-one correspondence between target points and polynomials. Thus, each ploynomial is in a sense \"single-use\" -- it is only used for the exact timestemp for which it is calculated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import signal\n",
    "\n",
    "tester.dea.pwm(0)\n",
    "center = 110.5\n",
    "\n",
    "xs = []\n",
    "ys = []\n",
    "ds = []\n",
    "ts = []\n",
    "\n",
    "# Target.\n",
    "def plot_sine_wave():\n",
    "    sinx = np.arange(0, tester.intervals['hold'] / tester.intervals['sample'], 1)\n",
    "    siny = 1 * np.sin(sinx / tester.intervals['hold'] * 2) + center\n",
    "    plt.plot(sinx, siny)\n",
    "    return sinx, siny\n",
    "    \n",
    "def plot_square_wave():\n",
    "    sinx = np.linspace(0, tester.intervals['hold'] / tester.intervals['sample'], 300, endpoint=True)\n",
    "    siny = signal.square(2 * np.pi * 2 * sinx) + center\n",
    "    plt.plot(sinx, siny)\n",
    "    return sinx, siny\n",
    "\n",
    "def plot_triangle_wave():\n",
    "    sinx = np.linspace(0, tester.intervals['hold'] / tester.intervals['sample'], 300, endpoint=True)\n",
    "    siny = signal.triang(2 * np.pi * 2 * sinx) + center\n",
    "    plt.plot(sinx, siny)\n",
    "    return sinx, siny\n",
    "\n",
    "sinx, siny = plot_sine_wave()\n",
    "\n",
    "# Actual.\n",
    "pwm = 0\n",
    "i = 0\n",
    "for y, poly in zip(siny, polys):\n",
    "    duty = round(poly(y), 2)\n",
    "    xs.append(y)\n",
    "    ys.append(max(min(duty, 100), -100))\n",
    "    print(\"center: {}, last displacement: {}, next displacement: {}; predicted duty cycle: {}\".format(round(center, 2), ds[-1] if ds else center, round(y, 2), duty))\n",
    "    tester.dea.pwm(duty)\n",
    "    time.sleep(tester.intervals['sample'])\n",
    "    ds.append(round(tester.sensor.distance(), 2))\n",
    "    ts.append(i)\n",
    "    i += 1\n",
    "    clear_output(wait=True)\n",
    "    \n",
    "tester.dea.pwm(0)\n",
    "\n",
    "plt.scatter(ts, ds);\n",
    "plt.title(\"Sine wave approximation\");\n",
    "plt.xlabel(\"timesteps ({} sec)\".format(tester.intervals['sample']));\n",
    "plt.ylabel(\"displacement (mm)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sinx = np.linspace(0, tester.intervals['hold'] // tester.intervals['sample'], tester.intervals['hold'] // tester.intervals['sample'], endpoint=True)\n",
    "siny = singal.triang(int(tester.intervals['hold'] // tester.intervals['sample']))\n",
    "plt.plot(sinx, siny)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_sine_wave()\n",
    "\n",
    "plt.scatter(ts, ds);\n",
    "plt.title(\"Sine wave approximation\");\n",
    "plt.xlabel(\"timesteps ({} sec)\".format(tester.intervals['sample']));\n",
    "plt.ylabel(\"displacement (mm)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "    sinx = np.linspace(0, tester.intervals['hold'] / tester.intervals['sample'], 100, endpoint=True)\n",
    "    siny = signal.square(2 * np.pi * 1 * sinx) + center\n",
    "    plt.plot(sinx, siny)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also measure the voltage output over time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(ts, ys);\n",
    "plt.title(\"Duty cycle for sine wave approximation\");\n",
    "plt.xlabel(\"timesteps ({} sec)\".format(tester.intervals['sample']));\n",
    "plt.ylabel(\"duty cycle (%)\");\n",
    "plt.ylim(bottom=0, top=105);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rmse = np.sqrt(((siny - ys) ** 2).mean())\n",
    "print(\"RMSE: {} mm\".format(rmse.round(3)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another interesting investigation is seeing how long it takes for the distance to change within a certain tolerance at a given duty cycle. This could also give valuable insight into hysteresis, and it could tell us how many polynomials we need for each duty cycle. We can use the power of Python here to customize the basic inner and outer loops that I have written."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Q-learning attempt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I will be using code from Chris Yoon's [excellent tutorial](https://towardsdatascience.com/dqn-part-1-vanilla-deep-q-networks-6eb4a00febfb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.autograd as autograd\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will again attempt to replicate the work that the Signapore folks have done.\n",
    "\n",
    "> In our RL control method, the $Q$-function $\\mu(x)$ is parametrized by a multilayer perceptron (MLP). We use a two hidden-layer network with 50 units each. ReLU is used as hidden activations, and Batch Normalization is applied after each hiddden layer. A fully-connected layer followed by Softmax layer is added after the second hidden layer, which generates a probability distribution over possible actions. At each timestep, the action with the highest probability is taken."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "class DQN(nn.Module):\n",
    "    def __init__(self, input_dim, output_dim):\n",
    "        super(DQN, self).__init__()\n",
    "        self.input_dim = input_dim\n",
    "        self.output_dim = output_dim\n",
    "        \n",
    "        self.fc = nn.Sequential(\n",
    "            nn.Linear(self.input_dim[0], 50),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(50, 50),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(50, 50),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(50, self.output_dim)\n",
    "        )\n",
    "\n",
    "    def forward(self, state):\n",
    "        qvals = self.fc(state)\n",
    "        return qvals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we have a replay buffer, which allows us to track the environment's response across time, and we can then pick mini-batched to train the resultant neural network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class BasicBuffer:\n",
    "    def __init__(self, max_size):\n",
    "        self.max_size = max_size\n",
    "        self.buffer = deque(maxlen=max_size)\n",
    "\n",
    "    def push(self, state, action, reward, next_state, done):\n",
    "        experience = (state, action, np.array([reward]), next_state, done)\n",
    "        self.buffer.append(experience)\n",
    "\n",
    "    def sample(self, batch_size):\n",
    "        state_batch = []\n",
    "        action_batch = []\n",
    "        reward_batch = []\n",
    "        next_state_batch = []\n",
    "        done_batch = []\n",
    "\n",
    "        batch = random.sample(self.buffer, batch_size)\n",
    "\n",
    "        for experience in batch:\n",
    "            state, action, reward, next_state, done = experience\n",
    "            state_batch.append(state)\n",
    "            action_batch.append(action)\n",
    "            reward_batch.append(reward)\n",
    "            next_state_batch.append(next_state)\n",
    "            done_batch.append(done)\n",
    "\n",
    "        return (state_batch, action_batch, reward_batch, next_state_batch, done_batch)\n",
    "\n",
    "    def __len__(self):\n",
    "        return len(self.buffer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For ease of plugging in various engines, let's phrase this task as a Gym environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "actions = [\n",
    "    0.1, 0.09, 0.08, 0.07. 0.06, 0.05, 0.04, 0.03, 0.02, 0.01. 0,\n",
    "    -0.04, -0.08, -0.12, -0.18, -0.24, -0.28, -0.32, -0.36, -0.40\n",
    "]\n",
    "\n",
    "# Convert to percent.\n",
    "10 * actions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "import gym\n",
    "\n",
    "# Episode data: 1000 actions per episode.\n",
    "# Episode ehds when we have fallen within the desired distance for some timesteps \n",
    "\n",
    "# What is the appropriate timestep for control? How often does the \n",
    "\n",
    "class DEAEnv(gym.Env):\n",
    "    def __init__(self, dea, ild, polys, actions):\n",
    "        self.__version__ = \"0.1.0\"\n",
    "        logging.info(f\"1D DEA Control Env - Version {self.__version__}\")\n",
    "        self.timestep = 0.1\n",
    "        self.dea = dea\n",
    "        self.ild = ild\n",
    "        self.polys = polys\n",
    "        \n",
    "        self.stale = False\n",
    "        self.actions = actions\n",
    "        self.action_space = gym.spaces.discrete(len(self.actions))\n",
    "        self.episodes = 0\n",
    "        self.time = 0\n",
    "        \n",
    "    def step(self, action):\n",
    "        if (self.time == len(polys)):\n",
    "            self.stale = True\n",
    "        \n",
    "        pwm = polys[self.time](action.d)\n",
    "        self.last_action = pwm\n",
    "        self.dea.pwm.write(pwm)\n",
    "        sleep(self.timestep)\n",
    "        \n",
    "        # Evaluate the polynomial \n",
    "        actual = round(self.ild.distance(), 4)\n",
    "        \n",
    "        return actual, abs(actual - action.d), false, {}\n",
    "    \n",
    "        self.time += 1\n",
    "    \n",
    "    def reset(self):\n",
    "        self.stale = False\n",
    "        self.dea.pwm(0)\n",
    "        self.time = 0\n",
    "        self.episodes += 1\n",
    "        sleep(10)\n",
    "        \n",
    "    def _reset(self):\n",
    "        pass\n",
    "\n",
    "    def _render(self, mode='human', close=False):\n",
    "        pass\n",
    "\n",
    "    def _take_action(self, action):\n",
    "        pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These are our actions, which PWM offsets from the estimated voltage above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class DQNAgent:\n",
    "    def __init__(self, env, learning_rate=3e-4, gamma=0.99, buffer_size=10000):\n",
    "        self.env = env\n",
    "        self.learning_rate = learning_rate\n",
    "        self.gamma = gamma\n",
    "        self.replay_buffer = BasicBuffer(max_size=buffer_size)\n",
    "\t\n",
    "        self.device = torch.device(\"cuda\" if torch.cuda.is_available() else \"cpu\")\n",
    "        self.model = DQN(env.observation_space.shape, env.action_space.n).to(self.device)\n",
    "\n",
    "        self.optimizer = torch.optim.Adam(self.model.parameters())\n",
    "        self.MSE_loss = nn.MSELoss()\n",
    "\n",
    "    def get_action(self, state, eps=0.20):\n",
    "        state = torch.FloatTensor(state).float().unsqueeze(0).to(self.device)\n",
    "        qvals = self.model.forward(state)\n",
    "        action = np.argmax(qvals.cpu().detach().numpy())\n",
    "        \n",
    "        if(np.random.randn() < eps):\n",
    "            return self.env.action_space.sample()\n",
    "\n",
    "        return action\n",
    "\n",
    "    def compute_loss(self, batch):\n",
    "        states, actions, rewards, next_states, dones = batch\n",
    "        states = torch.FloatTensor(states).to(self.device)\n",
    "        actions = torch.LongTensor(actions).to(self.device)\n",
    "        rewards = torch.FloatTensor(rewards).to(self.device)\n",
    "        next_states = torch.FloatTensor(next_states).to(self.device)\n",
    "        dones = torch.FloatTensor(dones)\n",
    "\n",
    "        curr_Q = self.model.forward(states).gather(1, actions.unsqueeze(1))\n",
    "        curr_Q = curr_Q.squeeze(1)\n",
    "        next_Q = self.model.forward(next_states)\n",
    "        max_next_Q = torch.max(next_Q, 1)[0]\n",
    "        expected_Q = rewards.squeeze(1) + self.gamma * max_next_Q\n",
    "\n",
    "        loss = self.MSE_loss(curr_Q, expected_Q)\n",
    "        return loss\n",
    "\n",
    "    def update(self, batch_size):\n",
    "        batch = self.replay_buffer.sample(batch_size)\n",
    "        loss = self.compute_loss(batch)\n",
    "\n",
    "        self.optimizer.zero_grad()\n",
    "        loss.backward()\n",
    "        self.optimizer.step()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I am following [this guide](https://towardsdatascience.com/building-neural-network-from-scratch-9c88535bf8e9) for the layer code."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
